import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;


public class App extends Application<Configuration> {
    @Override
    public void run(Configuration configuration, Environment environment) {
        environment.jersey().register(new AuctionRESTController());
        environment.jersey().register(new ItemRESTController());
        environment.jersey().register(new BidRESTController());
}

    public static void main(String args[]) throws Exception {

        new App().run(args);
        // SqlAPI tempSql = new SqlAPI();


    }

}