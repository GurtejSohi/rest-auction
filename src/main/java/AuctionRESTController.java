import RESTAuction.Auction;
import database.MemoryAPI;
import database.SqlAPI;

import javax.ws.rs.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/auction")
public class AuctionRESTController {

    @GET
    @Path("all")
    public Response getAllAuctions() {
        ArrayList<Auction> auctionList = SqlAPI.sqlAPI.getAllAuctions();
        return Response.ok(auctionList.toString()).build();
    }

    @PUT
    @Path("changeName/{id}/{name}")
    public Response changeAuctionName(@PathParam("id") long id, @PathParam("name") String name) {
        SqlAPI.sqlAPI.modifyAuctionName(id, name);
        return Response.ok().build();
    }

    @GET
    @Path("searchByName/{name}")
    public Response searchByName(@PathParam("name") String name) {
        Auction auction = SqlAPI.sqlAPI.searchByAuctionName(name);
        if (auction == null)
            return Response.ok("Nothing found").build();
        return Response.ok(auction.getAuctionId()).build();
    }

    @DELETE
    @Path("delete/{id}")
    public Response deleteAuction(@PathParam("id") long id) {
        SqlAPI.sqlAPI.deleteAuction(id);
        return Response.ok().build();
    }

    @POST
    @Path("create/{name}")
    public Response create(@PathParam("name") String name) {
        long id=SqlAPI.sqlAPI.getNewAuctionId();
        System.out.print("ID "+id);
        Auction auction = new Auction(id,name);
        SqlAPI.sqlAPI.createAuction(auction);
        return Response.ok(auction.getAuctionId()).build();
    }

}