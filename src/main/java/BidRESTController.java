import RESTAuction.Bid;
import RESTAuction.Item;
import database.MemoryAPI;
import database.SqlAPI;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/bid")
public class BidRESTController {
    @GET
    @Path("/listBids/{itemid}")
    public Response listBidsinItem(@PathParam("itemid") long itemid){
        Item item = SqlAPI.sqlAPI.getItemById(itemid);
        if (item == null)
            return Response.ok("No such item found").build();
        List<Bid> bidList=SqlAPI.sqlAPI.listBidsinItem(item);
        return Response.ok(bidList.toString()).build();
    }
   @POST
    @Path("/CreateBid/{itemid}/{Quantity}/{Price}")
    public Response CreateBid(@PathParam("itemid") long itemid,@PathParam("Quantity") long Quantity,@PathParam("Price") double Price ){
        long id=SqlAPI.sqlAPI.getNewBidId();
        Bid bid=new Bid(id,Quantity,Price,itemid);
        Item item=SqlAPI.sqlAPI.getItemById(itemid);
        if (item == null)
            return Response.ok("No such item found").build();
        long retval=SqlAPI.sqlAPI.createBid(item,bid);
        return Response.ok(retval).build();
    }

}
