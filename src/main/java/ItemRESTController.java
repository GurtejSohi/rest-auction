import RESTAuction.Auction;
import RESTAuction.Item;
import database.MemoryAPI;
import database.SqlAPI;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.ArrayList;

@Path("/item")
public class ItemRESTController {
    @GET
    @Path("/{name}")
    public Response getAllItemsInAuction(@PathParam("name") String name) {
        Auction auction = SqlAPI.sqlAPI.searchByAuctionName(name);
        ArrayList<Item> items = SqlAPI.sqlAPI.listItemsinAuction(auction);
        String output;
        if (auction == null)
            output = "Invalid Auction Id";
        else
            output = items.toString();
        return Response.ok(output).build();
    }

    @POST
    @Path("{id}/{name}/{quant}")
    public Response createItem(@PathParam("id") long id, @PathParam("name") String name, @PathParam("quant") long quant){
       long newItemId=SqlAPI.sqlAPI.getNewItemId();

        Auction auction = SqlAPI.sqlAPI.getAuctionById(id);
        String output;
        if (auction == null) output = "Invalid Auction Id";
        else {
            Item item = new Item(newItemId,name, quant, id);
            long itemId = SqlAPI.sqlAPI.addItemtoAuction(auction, item);
            output = Long.toString(itemId);
        }
        return Response.ok(output).build();
    }

    @GET
    @Path("{id}/searchByName/{name}")
    public Response searchByName(@PathParam("id") long id, @PathParam("name") String name) {
        Auction auction = SqlAPI.sqlAPI.getAuctionById(id);
        String output;
        if (auction == null) output = "Invalid Auction Id";
        else {
            Item item = SqlAPI.sqlAPI.searchIteminAuction(auction, name);
            output = Long.toString(item.getId());
        }
        return Response.ok(output).build();
    }

    @DELETE
    @Path("deleteItem/{auctionId}/{itemId}")
    public Response deleteItem(@PathParam("auctionId") long auctionId, @PathParam("itemId") long itemId) {
        Auction auction = SqlAPI.sqlAPI.getAuctionById(auctionId);
        String output;
        if (auction == null) output = "Invalid Auction Id";
        else {
            SqlAPI.sqlAPI.deleteItemfromAuction(auction, itemId);
            output = "Deleted item successfully";
        }
        return Response.ok(output).build();
    }

}
