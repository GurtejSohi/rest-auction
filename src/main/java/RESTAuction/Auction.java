package RESTAuction;

import javax.ws.rs.Path;
import java.util.Objects;

@Path("/")
public class Auction {
    String AuctionName;
    Long AuctionId;
    long timestamp;

    public String getAuctionName() {
        return AuctionName;
    }

    public void setAuctionName(String auctionName) {
        AuctionName = auctionName;
    }

    public Long getAuctionId() {
        return AuctionId;
    }

    public void setAuctionId(Long auctionId) {
        AuctionId = auctionId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Auction(long id,String name){
        this.AuctionName=name;

        this.AuctionId = id;
        this.timestamp=System.currentTimeMillis();
    }
    public Auction(String name, Long id, Long timestamp){
        this.AuctionName=name;

        this.AuctionId=id;
        this.timestamp=timestamp;
    }




    @Override
    public String toString() {
        return "Auction{" +
                "AuctionName='" + AuctionName + '\'' +
                ", AuctionId=" + AuctionId +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auction auction = (Auction) o;
        return Objects.equals(AuctionId, auction.AuctionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(AuctionId);
    }
}
