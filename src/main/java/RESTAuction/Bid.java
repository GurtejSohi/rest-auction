package RESTAuction;

import java.util.Objects;

public class Bid {
    long id;
    long quantity;
    double price;
    long itemId;
    long timestamp;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bid bid = (Bid) o;
        return id == bid.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", price=" + price +
                ", itemId=" + itemId +
                ", timestamp=" + timestamp +
                '}';
    }

    public Bid(long id,long quantity, double price, long itemId) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.itemId = itemId;
        this.timestamp = System.currentTimeMillis();
    }

    public Bid(long id,long quantity, double price, long itemId,long timestamp) {
        this.id =id;
        this.quantity = quantity;
        this.price = price;
        this.itemId = itemId;
        this.timestamp = timestamp;
    }
    public long getId() {
        return id;
    }

    public long getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public long getItemId() {
        return itemId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
