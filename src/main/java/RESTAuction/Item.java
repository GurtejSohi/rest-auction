package RESTAuction;

import java.util.Objects;

public class Item {
    long id;
    String name;
    long timestamp;
    long quantity;
    long auctionId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", timestamp=" + timestamp +
                ", quantity=" + quantity +
                ", auctionId=" + auctionId +
                '}';
    }

    public Item(long id,String name, long quantity, long auctionId) {
        this.id =id;
        this.name = name;
        this.timestamp = System.currentTimeMillis();
        this.quantity = quantity;
        this.auctionId = auctionId;
    }
    public Item(long id,String name, long quantity, long auctionId,long timestamp) {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.quantity = quantity;
        this.auctionId = auctionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(long auctionId) {
        this.auctionId = auctionId;
    }
}