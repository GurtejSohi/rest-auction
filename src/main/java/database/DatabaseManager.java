package database;

import RESTAuction.Auction;
import RESTAuction.Bid;
import RESTAuction.Item;

import java.util.ArrayList;

public interface DatabaseManager {
    long getNewAuctionId();
    long getNewItemId();
    long getNewBidId();
    Auction getAuctionById(long id);
    Item getItemById(long id);
    Bid getBidById(long id);


    ArrayList<Auction> getAllAuctions();
    long createAuction(Auction auction);
    boolean deleteAuction(long id);
    Auction searchByAuctionName (String name);
    void modifyAuctionName(long id, String new_name);

    long addItemtoAuction(Auction auction, Item item);
    ArrayList<Item> listItemsinAuction(Auction auction);
    Item searchIteminAuction(Auction auction, String name);
    boolean deleteItemfromAuction(Auction auction, long id);

    long createBid(Item item, Bid bid);
    ArrayList<Bid> listBidsinItem(Item item);
}

