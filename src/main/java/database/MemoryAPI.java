package database;

import RESTAuction.Auction;
import RESTAuction.Bid;
import RESTAuction.Item;

import java.util.ArrayList;

public class MemoryAPI implements DatabaseManager {

    public static MemoryAPI memoryAPI = new MemoryAPI();

     ArrayList<Auction> auctions=new ArrayList<>();

     ArrayList<Item> items=new ArrayList<>();

     ArrayList<Bid> bids = new ArrayList<>();

    public ArrayList<Auction> getAllAuctions(){
        return auctions;
    }


    @Override
    public long getNewAuctionId() {
        return auctions.size()+1;
    }

    @Override
    public long getNewItemId() {
        return items.size()+1;
    }

    @Override
    public long getNewBidId() {
        return bids.size()+1;
    }

    public Auction getAuctionById(long id){
        for (int i = 0; i < auctions.size(); i++) {
            Auction tempAuction = auctions.get(i);
            if (tempAuction.getAuctionId() == id) {
                return tempAuction;
            }
        }
        return null;
    }

    public Item getItemById(long id){
        for (int i = 0; i < items.size(); i++) {
            Item tempItem = items.get(i);
            if (tempItem.getId() == id) {
                return tempItem;
            }
        }
        return null;
    }
    public Bid getBidById(long id){
        for (int i = 0; i < bids.size(); i++) {
            Bid tempBid = bids.get(i);
            if (tempBid.getId() == id) {
                return tempBid;
            }
        }
        return null;
    }


    public long createAuction(Auction auction){
        auctions.add(auction);
        return auction.getAuctionId();

    }


    public boolean deleteAuction(long id) {
        for (int i = 0; i < auctions.size(); i++) {
            Auction TempAuction = auctions.get(i);
         Long tempId = TempAuction.getAuctionId();
         if(tempId.equals(id)){
             auctions.remove(i);
             return true;
         }
        }
        return false;
    }


    public Auction searchByAuctionName(String name){
        for (int i=0;i<auctions.size();i++){
            Auction TempAuction = auctions.get(i);
            if(TempAuction.getAuctionName().equals(name)){
                return TempAuction;

            }
        }
        return null;
    }

    public void modifyAuctionName(long id, String new_name){
        for (int i=0;i<auctions.size();i++) {
            Auction TempAuction = auctions.get(i);
            if (TempAuction.getAuctionId().equals(id)) {
                TempAuction.setAuctionName(new_name);

            }
        }
    }

    public long addItemtoAuction(Auction auction, Item item) {
        item.setAuctionId(auction.getAuctionId());
        items.add(item);
        return item.getId();
    }


    public ArrayList<Item> listItemsinAuction(Auction auction) {
        ArrayList<Item> tempItems = new ArrayList<>();
        for(int i=0;i<items.size();i++){
            Item tempItem = items.get(i);
            if(tempItem.getAuctionId()==auction.getAuctionId()){
                tempItems.add(tempItem);
            }
        }
        return tempItems;

    }


    public Item searchIteminAuction(Auction auction, String name) {
        for(int i=0;i<items.size();i++){
            Item TempItem = items.get(i);
            if(TempItem.getAuctionId()==auction.getAuctionId()){
                if(TempItem.getName().equals(name)){
                    return TempItem;
                }
            }
        }
        return null;
    }

    public boolean deleteItemfromAuction(Auction auction, long id) {
        for(int i=0;i<items.size();i++){
            Item TempItem = items.get(i);
            if(TempItem.getId()==id){
                items.remove(i);
                return true;
            }
        }
       return false;
    }

    public long createBid(Item item, Bid bid) {
        bid.setItemId(item.getId());
        bids.add(bid);
        return bid.getId();

    }

    public ArrayList<Bid> listBidsinItem(Item item) {
        ArrayList tempBids = new ArrayList();
       for(int i=0;i<bids.size();i++){
           Bid tempBid = bids.get(i);
           if(tempBid.getItemId()==item.getId()){
               tempBids.add(tempBid);
           }
       }
       return tempBids;

    }


}
