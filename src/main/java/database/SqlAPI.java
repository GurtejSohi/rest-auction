package database;

import RESTAuction.Auction;
import RESTAuction.Bid;
import RESTAuction.Item;

import java.sql.*;
import java.util.ArrayList;

public class SqlAPI implements DatabaseManager {

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "singcontroller";
    Connection con ;
    Statement stmt ;
    ResultSet rst;

    public static SqlAPI sqlAPI = new SqlAPI();

    public SqlAPI() {
        this.con=null;
        this.stmt=null;
        this.rst=null;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306?autoReconnect=true&useSSL=false","root",PASS);

            stmt = con.createStatement();
                String createDB="create database if not exists restauction";
                stmt.executeUpdate(createDB);
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/restauction?autoReconnect=true&useSSL=false","root",PASS);
            stmt = con.createStatement();

            String tempAuction="auction" +
                    "(id BIGINT  NOT NULL," +
                    "name VARCHAR(255) ," +
                    "timestamp BIGINT," +
                    "PRIMARY KEY ( id ) ) ;";

           String createTable = "create table if not exists "+tempAuction;
            stmt.execute(createTable);

            String tempItemp ="Item" +
                    "(id BIGINT  NOT NULL," +
                    "name VARCHAR(255) NOT NULL," +
                    "timestamp BIGINT NOT NULL," +
                    "quantity BIGINT NOT NULL,"+
                    "auctionid BIGINT NOT NULL,"+
                    "PRIMARY KEY ( id ) ) ;";
            createTable = "create table if not exists "+tempItemp;
            stmt.execute(createTable);

            String tempBid="Bid" +
                    "(id BIGINT  NOT NULL," +
                    "quantity BIGINT NOT NULL,"+
                    "price double NOT NULL,"+
                    "itemid BIGINT NOT NULL,"+
                    "timestamp BIGINT NOT NULL," +
                    "PRIMARY KEY ( id ) ) ;";
            createTable = "create table if not exists "+tempBid;
            stmt.executeUpdate(createTable);

        }catch(SQLException se){


            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){

            //Handle errors for Class.forName
            e.printStackTrace();
        }

    }




    public boolean truncateTable(String tablename){
        String deleteTable = "truncate table  "+tablename;
        try{
            Integer ids =stmt.executeUpdate(deleteTable);
            return true;
        }  catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public long getNewAuctionId(){
        String getNewId = "Select Max(id) as 'id' from Auction";

        try{
            rst= stmt.executeQuery(getNewId);
            rst.next();
            long id= rst.getLong("id")+1;
            rst.close();
         return id;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return 0;
    }

    public long getNewItemId(){
        String getNewId = "Select Max(id)as 'id' from item";

        try{
            rst= stmt.executeQuery(getNewId);
            rst.next();
            long id= rst.getLong("id")+1;
            rst.close();
            return id;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return 0;
    }

    public long getNewBidId(){
        String getNewId = "Select Max(id) as 'id' from Bid";

        try{
            rst= stmt.executeQuery(getNewId);
            rst.next();
            long id= rst.getLong("id")+1;
            rst.close();
            return id;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Auction getAuctionById(long id) {
        String selectAuctionByid="Select id, name,timestamp from Auction where id="+id+";";
        try{
            rst= stmt.executeQuery(selectAuctionByid);
            rst.next();
            Auction tempAuction = new Auction(rst.getString("name"), rst.getLong("id"),rst.getLong("timestamp"));

           rst.close();
           return tempAuction;
        }catch(SQLException se){
            //Handle errors for JDBC
            return null;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Item getItemById(long id) {
        String selectItemById = "Select name,timestamp,auctionid,quantity from item where id="+id+";";
        try{
            rst=stmt.executeQuery(selectItemById);
            if (!rst.next()) {
                return null;
            }            Item tempItem = new Item(id,rst.getString("name"),rst.getInt("quantity"),rst.getInt("auctionid"),rst.getLong("timestamp"));
            rst.close();
            return tempItem;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Bid getBidById(long id) {
        String selectItemById = "Select quantity,timestamp,itemid,price from bid where id="+id+";";
        try{
            rst=stmt.executeQuery(selectItemById);
            if (!rst.next()) {
                return null;
            }            Bid tempBid = new Bid(id,rst.getInt("quantity"),rst.getInt("price"),rst.getInt("itemid"),rst.getLong("timestamp"));
            rst.close();
            return tempBid;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Auction> getAllAuctions() {
        ArrayList<Auction>auctions = new ArrayList<>();
        String selectAuctionByid="Select * from auction ;";
        try {
            rst = stmt.executeQuery(selectAuctionByid);
            while (rst.next()) {
               Auction tempAuction = new Auction(rst.getString("name"), rst.getLong("id"), rst.getLong("timestamp"));
            auctions.add(tempAuction);
            }
            rst.close();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return auctions;
    }

    @Override
    public long createAuction(Auction auction) {
        long id=auction.getAuctionId();
        String name=auction.getAuctionName();
        Long timestamp = auction.getTimestamp();

        try {
            String insertAuction = "Insert INTO Auction values(" + id + ",'" + name + "'," + timestamp + ");";
            stmt.executeUpdate(insertAuction);
            return auction.getAuctionId();
        }
catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public boolean deleteAuction(long id) {
        try {
            String insertAuction = "Delete From Auction where id="+id+";";
            stmt.executeUpdate(insertAuction);
            return true;
        }
        catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Auction searchByAuctionName(String name) {

        String selectAuctionByid="Select id,name,timestamp from Auction where name='"+name+"';";
        try{
            rst= stmt.executeQuery(selectAuctionByid);
            if (!rst.next()) {
                return null;
            }
            Auction tempAuction =new Auction(rst.getString("name"),rst.getLong("id"),rst.getLong("timestamp"));
            rst.close();
            return tempAuction;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void modifyAuctionName(long id, String new_name) {
        try {
            String insertAuction = "Update  Auction set name='"+new_name+"' where id="+id+";";
            stmt.executeUpdate(insertAuction);

        }
        catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }


    }

    @Override
    public long addItemtoAuction(Auction auction, Item item) {
        long id = item.getId();
        long auctionId = item.getAuctionId();
        String name = item.getName();
        long quantity = item.getQuantity();
        long timestamp = item.getTimestamp();

        try {
            String insertAuction = "Insert INTO item values(" + id + ",'" + name + "'," +timestamp+","+ quantity +","+auctionId+ ");";
            stmt.executeUpdate(insertAuction);
            return item.getId();
        }
        catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return -1;

    }

    @Override
    public ArrayList<Item> listItemsinAuction(Auction auction) {
        ArrayList<Item> items = new ArrayList<>();
        String selectItemsByid="Select * from item where auctionid="+auction.getAuctionId()+" ;";
        try {
            rst = stmt.executeQuery(selectItemsByid);
            while (rst.next()) {
                Item tempItem = new Item(rst.getInt("id"),rst.getString("name"), rst.getInt("quantity"),auction.getAuctionId(), rst.getLong("timestamp"));
                items.add(tempItem);
            }
            rst.close();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return items;


    }

    @Override
    public Item searchIteminAuction(Auction auction, String name) {
        String selectItemByAunctionId = "Select * from item where auctionid="+auction.getAuctionId()+" and name='"+name+"';";
        try{
            System.out.print(selectItemByAunctionId);
            rst=stmt.executeQuery(selectItemByAunctionId);
            rst.next();
            Item tempItem = new Item(rst.getInt("id"),rst.getString("name"), rst.getInt("quantity"),auction.getAuctionId(), rst.getLong("timestamp"));
            rst.close();
            return tempItem;
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteItemfromAuction(Auction auction, long id) {
        try {
            String deleteItem = "Delete From item where id="+id+" and auctionid="+auction.getAuctionId()+";";
            stmt.executeUpdate(deleteItem);
            return true;
        }
        catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public long createBid(Item item, Bid bid) {
       long id = bid.getId();
       long quantity =bid.getQuantity();
       double price = bid.getPrice();
       long itemId = item.getId();
       long timestamp = bid.getTimestamp();

//       long price = bid.getPrice();

        try {
            String insertAuction = "Insert INTO bid values(" + id + "," + quantity + "," +price+","+itemId+","+ timestamp+ ");";
            stmt.executeUpdate(insertAuction);
            return bid.getId();
        }
        catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public ArrayList<Bid> listBidsinItem(Item item) {
        ArrayList<Bid>bids = new ArrayList<>();
        String selectBidsByid="Select * from bid where itemid="+item.getId()+" ;";
        try {
            rst = stmt.executeQuery(selectBidsByid);
            while (rst.next()) {
                Bid tempBid = new Bid(rst.getInt("id"),rst.getInt("quantity"), rst.getDouble("price"),rst.getInt("itemid"), rst.getLong("timestamp"));
                bids.add(tempBid);
            }
            rst.close();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        return bids;
    }
}
