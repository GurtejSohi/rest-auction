import RESTAuction.Auction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class AuctionRESTControllerTest {
    private AuctionRESTController resource;

    @Before
    public void setUp() throws Exception {
        resource = new AuctionRESTController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getAllAuctions() {
        Response response = resource.getAllAuctions();
        assertEquals(200, response.getStatus());
        System.out.println(response.toString());
    }

    @Test
    public void changeAuctionName() {
        Response response = resource.changeAuctionName(1, "asd");
        assertEquals(200, response.getStatus());
        System.out.println(response.toString());
    }

    @Test
    public void searchByName() {
        Response response = resource.searchByName("asd");
        assertEquals(200, response.getStatus());
        System.out.println(response.toString());
    }

    @Test
    public void deleteAuction() {
        Response response = resource.deleteAuction(3);
        assertEquals(200, response.getStatus());
        System.out.println(response.toString());
    }

    @Test
    public void create() {
        Response response = resource.create("asdasd");
        assertEquals(200, response.getStatus());
        System.out.println(response.toString());
    }
}