package RESTAuction;

import org.junit.Test;

import static org.junit.Assert.*;

public class AuctionTest {

    @Test
    public void getAuctionName() {
        Auction tempAuction =new Auction(1,"Sunil");
        assertEquals(tempAuction.getAuctionName(),"Sunil");

    }

    @Test
    public void setAuctionName() {
        Auction tempAuction = new Auction(1,"abc");
        tempAuction.setAuctionName("xyz");
        assertEquals(tempAuction.getAuctionName(),"xyz");
    }

    @Test
    public void getAuctionId() {
        Auction tempAuction = new Auction(2,"abc");
        assert(tempAuction.getAuctionId()==2);
    }

    @Test
    public void setAuctionId() {
        Auction tempAuction = new Auction(2,"abx");
        tempAuction.setAuctionId(new Long(5));
        assert(tempAuction.getAuctionId()==5);
    }

    @Test
    public void getTimestamp() {
        long timestamp = System.currentTimeMillis();
        Auction tempAuction = new Auction("abc",new Long(2),timestamp);
        assertEquals(tempAuction.getTimestamp(),timestamp);
    }

    @Test
    public void setTimestamp() {
        long timestamp = System.currentTimeMillis();
        Auction tempAuction = new Auction("xyz",new Long(3),timestamp);
        assertEquals(tempAuction.getTimestamp(),timestamp);
        long newTimestamp = System.currentTimeMillis();
//        assert (newTimestamp>=timestamp);
        tempAuction.setTimestamp(newTimestamp);
        assertEquals(tempAuction.getTimestamp(),newTimestamp);

    }

//    @Test
//    public void toString() {
//    }

    @Test
    public void equals() {
    }

//    @Test
//    public void hashCode() {
//    }
}