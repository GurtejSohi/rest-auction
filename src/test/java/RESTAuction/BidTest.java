package RESTAuction;

import org.junit.Test;

import static org.junit.Assert.*;

public class BidTest {

    @Test
    public void getId() {
        Bid bid = new Bid(2,15,500.5,1);
        assertEquals(bid.getId(),2);
    }

    @Test
    public void getQuantity() {
        Bid bid = new Bid(1,10,505.5,2);
        assertEquals(bid.getQuantity(),10);
    }

    @Test
    public void getPrice() {
        Bid bid = new Bid(3,11,51.5,3);
        assertEquals(bid.getPrice(),51.5, 0.1);
    }

    @Test
    public void getItemId() {
        Bid bid = new Bid(4,12,56.6,4);
        assertEquals(bid.getItemId(),4);
    }

    @Test
    public void getTimestamp() {
        long timestamp =System.currentTimeMillis();
        Bid bid = new Bid(5,13,57.5,5,timestamp);
        assertEquals(bid.getTimestamp(),timestamp);

    }

    @Test
    public void setId() {
        Bid bid = new Bid(6,14,58.5,6);
        bid.setId(new Long(7));
        assertEquals(bid.getId(),7);
    }

    @Test
    public void setQuantity() {
        Bid bid = new Bid(7,15,59.5,7);
        bid.setQuantity(new Long(16));
        assertEquals(bid.getQuantity(),16);
    }

    @Test
    public void setPrice() {
        Bid bid = new Bid(9,17,60.5,8);
        bid.setPrice(61.5);
        assertEquals(bid.getPrice(),61.5, 0.1);
    }

    @Test
    public void setItemId() {
        Bid bid = new Bid(10,17,61.5,9);
        bid.setItemId(10);
        assertEquals(bid.getItemId(),10);
    }
}