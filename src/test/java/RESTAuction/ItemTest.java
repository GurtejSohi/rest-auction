package RESTAuction;

import org.junit.Test;

import static org.junit.Assert.*;

public class ItemTest {

    @Test
    public void getId() {
        Item item = new Item(3,"car",55,1);
        assertEquals(item.getId(),3);
    }

    @Test
    public void setId() {
        Item item = new Item(1,"cat",33,2);
        item.setId(new Long(2));
        assertEquals(item.getId(),2);
    }

    @Test
    public void getName() {
        Item item = new Item(4,"ball",22,4);
        assertEquals(item.getName(),"ball");
    }

    @Test
    public void setName() {
        Item item = new Item(5,"apple",12,3);
        item.setName("pint");
        assertEquals(item.getName(),"pint");

    }

    @Test
    public void getTimestamp() {
        long timestamp = System.currentTimeMillis();
        Item item = new Item(3,"dog",2,1,timestamp);
        assertEquals(item.getTimestamp(),timestamp);
    }

    @Test
    public void setTimestamp() {
        Item item = new Item(6,"cellphone",33,2);
        long timestamp = System.currentTimeMillis();
        item.setTimestamp(timestamp);
        assertEquals(item.getTimestamp(),timestamp);
    }

    @Test
    public void getQuantity() {
        Item item = new Item(7,"yahoo",25,3);
        assertEquals(item.getQuantity(),25);
    }

    @Test
    public void setQuantity() {
        Item item = new Item(8,"lol",565,2);
        item.setQuantity(new Long(7));
        assertEquals(item.getQuantity(),7);
    }

    @Test
    public void getAuctionId() {
        Item item = new Item(9,"hat",40,1);
        assertEquals(item.getAuctionId(),1);
    }

    @Test
    public void setAuctionId() {
        Item item = new Item(10,"eyes",20,2);
        item.setAuctionId(new Long(1));
        assertEquals(item.getAuctionId(),1);
    }
}