package database;

import RESTAuction.Auction;
import RESTAuction.Bid;
import RESTAuction.Item;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MemoryAPITest {

    private MemoryAPI memoryAPI;

    @Before
    public void setUp() {
        memoryAPI = new MemoryAPI();
    }

    @Test
    public void getAllAuctions() {
        ArrayList<Auction> auctions = memoryAPI.getAllAuctions();
        assertEquals(0, auctions.size());
    }

    @Test
    public void createAuction() {
        Auction testAuction = new Auction(1, "test");
        Long id = memoryAPI.createAuction(testAuction);
        assertEquals(testAuction.getAuctionId(), id);
        ArrayList<Auction> auctions = memoryAPI.getAllAuctions();
        assertNotNull(auctions);
        assertEquals(1, auctions.size());
        assertEquals(id, auctions.get(0).getAuctionId());
    }

    @Test
    public void getAuctionById() {
        Auction fetchAuction = memoryAPI.getAuctionById(10); // Non existent
        assertNull(fetchAuction);

        Long id = memoryAPI.createAuction(new Auction(1, "test"));
        fetchAuction = memoryAPI.getAuctionById(id);
        assertEquals(id, fetchAuction.getAuctionId());
    }

    @Test
    public void searchByAuctionName() {
        Auction fetchAuction = memoryAPI.searchByAuctionName("non existing auction"); // Non existent
        assertNull(fetchAuction);

        Long id = memoryAPI.createAuction(new Auction(1, "test"));
        fetchAuction = memoryAPI.searchByAuctionName("test");
        assertNotNull(fetchAuction);
        assertEquals(id, fetchAuction.getAuctionId());
    }

    @Test
    public void deleteAuction() {
        Boolean val = memoryAPI.deleteAuction(10); // Non existent
        assertFalse(val);

        Long id = memoryAPI.createAuction(new Auction(1, "test"));
        val = memoryAPI.deleteAuction(id);
        assertTrue(val);
        assertNull(memoryAPI.getAuctionById(id));
    }


    @Test
    public void modifyAuctionName() {
        Long id = memoryAPI.createAuction(new Auction(1, "test"));
        memoryAPI.modifyAuctionName(id, "test2");
        Auction fetchAuction = memoryAPI.getAuctionById(id);
        assertEquals("test2", fetchAuction.getAuctionName());
    }

    @Test
    public void getItemById() {
        Item item=memoryAPI.getItemById(99); //Non Existent
        assertNull(item);

        Auction auc=new Auction(1, "test");
        long id1=memoryAPI.addItemtoAuction(auc,new Item(26,"itemname",5,auc.getAuctionId()));
        item=memoryAPI.getItemById(id1);
        assertEquals(item.getId(),id1);



    }

    @Test
    public void addItemtoAuction() {
        Auction auc=new Auction(11,"test");
        Item item=new Item(25,"itemname",5,auc.getAuctionId());
        Long id=memoryAPI.addItemtoAuction(auc,item);
        assertEquals(memoryAPI.getItemById(id),item);
    }
    @Test
    public void createBid() {
        Auction auc=new Auction(12,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(5,5,5,item.getId());
        long id=memoryAPI.createBid(item,testBid);
        assertEquals(id,testBid.getId());
    }

    @Test
    public void getBidById() {
        Bid fetchBid=memoryAPI.getBidById(99); //NonExistent
        assertNull(fetchBid);

        Auction auc=new Auction(172,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(55,5,5,item.getId());
        memoryAPI.createBid(item,testBid);
        assertEquals(testBid,memoryAPI.getBidById(testBid.getId()));
    }


    @Test
    public void listItemsinAuction() {
        Auction auc=new Auction(121,"test");
        ArrayList<Item> tempItems =memoryAPI.listItemsinAuction(auc);
        assertEquals(0,tempItems.size());
        Item item=new Item(29,"itemname",5,auc.getAuctionId());
        long id=memoryAPI.addItemtoAuction(auc,item);
        tempItems =memoryAPI.listItemsinAuction(auc);
        assertEquals(1,tempItems.size());


    }

    @Test
    public void searchIteminAuction() {
        Auction auc=new Auction(101,"test");
        Item item=new Item(49,"itemname",5,auc.getAuctionId());
        long id=memoryAPI.addItemtoAuction(auc,item);
        assertEquals(item,memoryAPI.searchIteminAuction(auc,"itemname"));


    }

    @Test
    public void deleteItemfromAuction() {
        Auction auc=new Auction(181,"test");
        Item item=new Item(59,"itemname",5,auc.getAuctionId());
        long id=memoryAPI.addItemtoAuction(auc,item);
        assertTrue(memoryAPI.deleteItemfromAuction(auc,id));
    }

    @Test
    public void listBidsinItem() {
        Auction auc=new Auction(124,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(5,5,5,item.getId());
        long id=memoryAPI.createBid(item,testBid);
        ArrayList<Bid> bids=memoryAPI.listBidsinItem(item);
        assertEquals(1,bids.size());

    }
}