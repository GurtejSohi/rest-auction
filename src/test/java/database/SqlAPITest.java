package database;

import RESTAuction.Auction;
import RESTAuction.Bid;
import RESTAuction.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SqlAPITest {

    private static SqlAPI sqlAPI;

    @BeforeClass
    public static void setUp() throws Exception {
        sqlAPI = new SqlAPI();
        sqlAPI.truncateTable("auction");
        sqlAPI.truncateTable("item");
        sqlAPI.truncateTable("bid");
    }

    @After
    public void tearDown() throws Exception {
        sqlAPI.truncateTable("auction");
        sqlAPI.truncateTable("item");
        sqlAPI.truncateTable("bid");
    }

    @Test
    public void getAllAuctions() {
        ArrayList<Auction> auctions = sqlAPI.getAllAuctions();
        assertEquals(0, auctions.size());
    }

    @Test
    public void createAuction() {
        Auction testAuction = new Auction(1, "test");
        Long id = sqlAPI.createAuction(testAuction);
        assertEquals(testAuction.getAuctionId(), id);
        ArrayList<Auction> auctions = sqlAPI.getAllAuctions();
        assertNotNull(auctions);
        assertEquals(1, auctions.size());
        assertEquals(id, auctions.get(0).getAuctionId());
    }

    @Test
    public void getAuctionById() {
        Auction fetchAuction = sqlAPI.getAuctionById(10); // Non existent
        assertNull(fetchAuction);

        Long id = sqlAPI.createAuction(new Auction(1, "test"));
        fetchAuction = sqlAPI.getAuctionById(id);
        assertEquals(id, fetchAuction.getAuctionId());
    }

    @Test
    public void searchByAuctionName() {
        Auction fetchAuction = sqlAPI.searchByAuctionName("non existing auction"); // Non existent
        assertNull(fetchAuction);

        Long id = sqlAPI.createAuction(new Auction(1, "test"));
        fetchAuction = sqlAPI.searchByAuctionName("test");
        assertNotNull(fetchAuction);
        assertEquals(id, fetchAuction.getAuctionId());
    }

    @Test
    public void deleteAuction() {
        Long id = sqlAPI.createAuction(new Auction(1, "test"));
        Boolean val = sqlAPI.deleteAuction(id);
        assertTrue(val);
        assertNull(sqlAPI.getAuctionById(id));
    }


    @Test
    public void modifyAuctionName() {
        Long id = sqlAPI.createAuction(new Auction(1, "test"));
        sqlAPI.modifyAuctionName(id, "test2");
        Auction fetchAuction = sqlAPI.getAuctionById(id);
        assertEquals("test2", fetchAuction.getAuctionName());
    }

    @Test
    public void getItemById() {
        Item item=sqlAPI.getItemById(99); //Non Existent
        assertNull(item);

        Auction auc=new Auction(1, "test");
        long id1=sqlAPI.addItemtoAuction(auc,new Item(26,"itemname",5,auc.getAuctionId()));
        item=sqlAPI.getItemById(id1);
        assertEquals(item.getId(),id1);



    }

    @Test
    public void addItemtoAuction() {
        Auction auc=new Auction(11,"test");
        Item item=new Item(25,"itemname",5,auc.getAuctionId());
        Long id=sqlAPI.addItemtoAuction(auc,item);
        assertEquals(sqlAPI.getItemById(id),item);
    }
    @Test
    public void createBid() {
        Auction auc=new Auction(12,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(5,5,5,item.getId());
        long id=sqlAPI.createBid(item,testBid);
        assertEquals(id,testBid.getId());
    }

    @Test
    public void getBidById() {
        Bid fetchBid=sqlAPI.getBidById(99); //NonExistent
        assertNull(fetchBid);

        Auction auc=new Auction(172,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(55,5,5,item.getId());
        sqlAPI.createBid(item,testBid);
        assertEquals(testBid,sqlAPI.getBidById(testBid.getId()));
    }


    @Test
    public void listItemsinAuction() {
        Auction auc=new Auction(121,"test");
        ArrayList<Item> tempItems =sqlAPI.listItemsinAuction(auc);
        assertEquals(0,tempItems.size());
        Item item=new Item(29,"itemname",5,auc.getAuctionId());
        long id=sqlAPI.addItemtoAuction(auc,item);
        tempItems =sqlAPI.listItemsinAuction(auc);
        assertEquals(1,tempItems.size());


    }

    @Test
    public void searchIteminAuction() {
        Auction auc=new Auction(101,"test");
        Item item=new Item(49,"itemname",5,auc.getAuctionId());
        long id=sqlAPI.addItemtoAuction(auc,item);
        assertEquals(item,sqlAPI.searchIteminAuction(auc,"itemname"));


    }

    @Test
    public void deleteItemfromAuction() {
        Auction auc=new Auction(181,"test");
        Item item=new Item(59,"itemname",5,auc.getAuctionId());
        long id=sqlAPI.addItemtoAuction(auc,item);
        assertTrue(sqlAPI.deleteItemfromAuction(auc,id));
    }

    @Test
    public void listBidsinItem() {
        Auction auc=new Auction(124,"test");
        Item item=new Item(28,"itemname",5,auc.getAuctionId());
        Bid testBid=new Bid(5,5,5,item.getId());
        long id=sqlAPI.createBid(item,testBid);
        ArrayList<Bid> bids=sqlAPI.listBidsinItem(item);
        assertEquals(1,bids.size());

    }
}